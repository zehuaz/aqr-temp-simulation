import datetime
import matplotlib.pyplot as plt

# Global variables init
preTemp, ons, offs, heaterStatus = 40, 0, 0, False


def pid(sp, cv):
    """

    :rtype: int
    """
    sumErr, preVal = 0, 0
    # Something wrong with the coefficients, better results without Ki and Kd (from Cathy)
    p = 1.65
    # set i and d to zero
    i = 0
    d = 0

    # define set point and coefficients for PID
    err = sp - cv
    sumErr = sumErr + err

    # calculate each term in PID formula
    pterm = p * err
    iterm = i * sumErr
    dterm = d * (preVal - cv)

    # get sum of three terms
    res = pterm + iterm + dterm
    preVal = cv
    return res

def heaterFn():
    return heaterStatus

def getTemp(heaterFn):  # generator
    while True:
        heater = heaterFn
        hearterSlope = slope(heater)
        cv = preTemp + hearterSlope
        preTemp = cv
        yield cv


def slope(heater):  
    if (not heater):
        s = -0.556
    else:
        s = 0.526
    return s

def turnOnHeater(res):
    if res > 10:
        heater = True
    else:
        heater = False


def main():
    t = datetime.time
    sp = 50
    heater = 0
    cv = getTemp(heaterFn)  # don't know how to determine whether heater is on/off
    res = pid(sp, next(cv))
    turnOnHeater(res)
    # make x-axis as time, y-axis as data get from PID
    x, y, cvList = ([] for i in range(3))
    x.append(t)
    y.append(res)
    cvList.append(next(cv))  # use generator to add items to list which will hold all the current temperatures

    # plot PID graph in green and current temperature in red
    plt.plot(x, y, 'g')
    plt.plot(x, cvList, 'r')
    # beautify the x-labels
    plt.gcf().autofmt_xdate()
    # show the graph
    plt.show()


if __name__ == "__main__":
    main()